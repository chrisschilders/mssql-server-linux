# chrisschilders/mssql-server-linux

This image is an extension of the official [microsoft/mssql-server-linux](https://hub.docker.com/r/microsoft/mssql-server-linux/) docker image.
It adds functionality to initialize a mssql-server-linux container with .sh or .sql files.
When a container is started for the first time, it will execute any files with the extensions .sh or .sql that are found in the /docker-entrypoint-initdb.d directory.

## Running this image

With `docker run`
```
docker run -p 1433:1433 --name mssql -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Pass@word' -v $PWD/initdb.d:/docker-entrypoint-initdb.d -d chrisschilders/mssql-server-linux
```

With `docker-compose`
```
db:
    image: chrisschilders/mssql-server-linux
    environment:
      - ACCEPT_EULA=Y
      - SA_PASSWORD=Pass@word
    ports:
      - "1433:1433"
    volumes:
      - initdb.d:/docker-entrypoint-initdb.d/
```