#!/bin/bash
echo "$0: Starting SQL Server"

sed -i -e 's/\r$//' docker-entrypoint-initdb.sh
./docker-entrypoint-initdb.sh & /opt/mssql/bin/sqlservr
